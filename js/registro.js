// Editar producto
var editar = false;

window.onload = function() {
    var id = $.urlParam('id');
    console.log(id);
    if(id != null){
        editar = true;
        $("#txtidproducto").val(id);
        PintarProducto(id);
    }
};

// Método para obtener un parámetro específico de la url
$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
    return null;
    }
    return decodeURI(results[1]) || 0;
}

// Pintar el producto que se va a editar
function PintarProducto(idProducto){

    $.get("https://localhost:7191/api/Producto/" + idProducto)
    .done(function( response ) {
        console.log(response);
        $("#nameProduct").val(response.nombreProducto),
        $("#priceProduct").val(response.precio),
        $("#stockProduct").val(response.stock)
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
        console.error("Error en la solicitud AJAX:", textStatus, errorThrown);
        alert("Hubo un error al cargar los detalles del producto. Por favor, inténtalo de nuevo más tarde.");
    });
}


// Guardar producto
function GuardarProducto() {
// Editar producto en caso de que ya exista
if(editar) {
    var id = $("#txtidproducto").val();
    var data = {
        Id: id,
        NombreProducto : $("#nameProduct").val(),
        Precio : $("#priceProduct").val(),
        Stock : $("#stockProduct").val()
    }

    $.ajax({
    method: "PUT",
    url: "https://localhost:7191/api/Producto/" + id, // Incluir el id en la URL
    contentType: 'application/json',
    data: JSON.stringify(data),
    })
    .done(function( response ) {
        console.log(response);
        if (response && response.error) {
            alert("Error al Modificar el producto: " + response.error);
        } else {
            alert("Se guardaron los cambios");
            window.location = "index.html";
        }
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
        console.error("Error en la solicitud AJAX:", textStatus, errorThrown);
        alert("Hubo un error al guardar los cambios del producto. Por favor, inténtalo de nuevo más tarde.");
    });
    

} else { // Crear nuevo producto
    var data = {
        NombreProducto : $("#nameProduct").val(),
        Precio : $("#priceProduct").val(),
        Stock : $("#stockProduct").val()
    }

    $.ajax({
    method: "POST",
    url: "https://localhost:7191/api/Producto",
    contentType: 'application/json',
    data: JSON.stringify(data), 
    })
    .done(function( response ) {
        console.log(response);
        if(response){
            alert("Producto creado");
            window.location = "index.html";
        }else{
            alert("Error al crear producto")
        }
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
        console.error("Error en la solicitud AJAX:", textStatus, errorThrown);
        alert("Hubo un error al crear el producto. Por favor, inténtalo de nuevo más tarde.");
    });
}
}