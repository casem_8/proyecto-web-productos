window.onload = function() {
    Obtener();
    };

    // Función para redirigir al formulario de registro de producto
    function IrFormularioCrear(){
        window.location = "RegistrarProducto.html";
    }

    // Función para obtener la lista de productos
    function Obtener(){

        $(".table tbody").html("");

        $.get("https://localhost:7191/api/Producto")
        .done(function( response ) {
            console.log(response);
            $.each( response, function( id, fila ) {
                $("<tr>").append(
                    $("<td>").text(fila.id),
                    $("<td>").text(fila.nombreProducto),
                    $("<td>").text(fila.precio),
                    $("<td>").text(fila.stock),
                    $("<td>").append(
                        $("<button>").data("id",fila.id).addClass("btn btn-primary btn-sm editar").text("Editar").attr({"type":"button"}),
                        $("<button>").data("id",fila.id).addClass("btn btn-danger btn-sm eliminar").text("Eliminar").attr({"type":"button"})
                    )
                ).appendTo(".table");
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.error("Error en la solicitud AJAX:", textStatus, errorThrown);
                alert("No se pudo cargar la lista de productos. Por favor, inténtalo de nuevo más tarde.");
            });
        });
    }

    // Evento para el botón editar
    $(document).on('click', '.editar', function () {
        console.log($(this).data("id"));
        window.location = "RegistrarProducto.html?id=" + $(this).data("id"); 
    });

    // Evento para el botón eliminar
    $(document).on('click', '.eliminar', function () {
        console.log($(this).data("id"));

        $.ajax({
        method: "DELETE",
        url: "https://localhost:7191/api/Producto/" + $(this).data("id")
        })
        .done(function( response ) {
            console.log(response);
            if (response && response.error) {
                alert("Error al eliminar el producto: " + response.error);
            } else {
                alert("Producto eliminado con éxito");
                window.location = "index.html";
            }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            console.error("Error en la solicitud AJAX:", textStatus, errorThrown);
            alert("Hubo un error al eliminar el producto. Por favor, inténtalo de nuevo más tarde.");
        });    
    });